
import React, { Fragment, Component }  from 'react';
import { BrowserRouter, Switch, Redirect, Route, } from 'react-router-dom';
// import {UserListContainer, UserTestContainer} from './user/index';
// import {ProductListContainer} from './product/index';
// import {connect } from 'react-redux';
// import autobind from 'autobind-decorator';
// import {bindActionCreators} from 'redux';
import {productContainer, productDetailContainer} from './product/index'

const Routes = () => (
  <BrowserRouter basename="/">
    <Switch>
    <Redirect exact from="/" to="/front/product"/>
    {/* <Redirect exact from="/" to="/front/user"/> */}
      <Route path="/front" component={({match}) =>
        <Fragment>
          <Route path={`${match.path}/product`} component={productContainer} />
          {/* <Route path={`${match.path}/user`} component={UserListContainer} /> */}
        </Fragment>
      }/>
    {/* <Route path="/user" component={UserListContainer} />   */}
    {/* <Route path = "/user/detail/:id" component={UserTestContainer}/> */}
    </Switch>
  </BrowserRouter>
);

// const mapStateToProps = ({paramurlState}) => ({
//   paramurl: paramurlState.paramurl,
  // product: productState.product,
  // pricePolicies: pricePoliciesState.pricePolicies,
  // messages: messagesState.messages
// });
// const mapDispatchToProps = (dispatch) => ({
//   productAction: bindActionCreators(productAction, dispatch),
// });


export default Routes;
// export default connect (mapStateToProps, mapDispatchToProps)(Routes);
