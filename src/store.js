
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { routerReducer } from 'react-router-redux';
import thunk from 'redux-thunk';
import {productReducer, productsReducer, pricePoliciesReducer, messagesReducer, paramUrlReducer, paramUrlSettingReducer} from './product/index';
// import {userReducer} from './user/index';

const rootReducer = combineReducers({
  routing: routerReducer,
  productState: productReducer,
  productsState: productsReducer,
  pricePoliciesState: pricePoliciesReducer,
  messagesState: messagesReducer,
  paramurlState: paramUrlReducer,
  paramGetBoolState: paramUrlSettingReducer,
  // userState: userReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk),
  )
);
