
import React, { PureComponent } from 'react';
import { Header, Divider } from 'semantic-ui-react';

import ItemDetailItemView from './ItemDetailItemView';

class ItemDetailView extends PureComponent {
    //
    render() {
        //
        return (
            <div>
                <Header as='h2'>
                </Header>
                <Divider/>
                <ItemDetailItemView 
                />
            </div>
        );
    }
}

export default ItemDetailView;