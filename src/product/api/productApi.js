import axios from 'axios';


function funcFindAllProduct(){
    return axios.get('/product').then(((response) => response && response.data || null ));
}

function funcSaveProducts(product){
    // return axios.get('/product', product).then(((response) => response && response.data || null ));
    return axios.post('/product', product).then(((response) => {
        return(response && response.data || null)
    }));
}

export default {
    funcFindAllProduct, funcSaveProducts
}