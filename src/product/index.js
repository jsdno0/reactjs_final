
import productReducer from './reducer/productReducer';

import productsReducer from './reducer/productReducer';

import pricePoliciesReducer from './reducer/productReducer';

import productAction from './action/productAction';

import productContainer from './container/productContainer';

import productDetailContainer from './container/productDetailContainer';

import messagesReducer from './reducer/productReducer';

import paramUrlReducer from './reducer/productReducer';

import paramUrlSettingReducer from './reducer/productReducer'

export default {
    productReducer,
    productsReducer,
    messagesReducer,
    productAction,
    pricePoliciesReducer,
    productContainer,
    productDetailContainer,
    paramUrlReducer,
    paramUrlSettingReducer,
}

export {
    productReducer,
    productsReducer,
    messagesReducer,
    productAction,
    pricePoliciesReducer,
    productContainer,
    productDetailContainer,
    paramUrlReducer,
    paramUrlSettingReducer,
}