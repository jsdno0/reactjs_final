const actionType = {
    SET_PRODUCT: 'product.setProduct',
    SET_PRODUCTS: 'product.setProducts',
    SET_PRICE_POLICIES: 'product.setPricePolicies',
    CHANGE_PRODUCT_PROP: 'product.changeProductProp',
    CHANGE_PRICE_POLICIES: 'product.changePricePolicies',
    CHANGE_PRODUCT_PRICE_POLICIES: 'product.changeProductPricePolicies',
    COMPARE_DATA_TO_FROM : 'product.compareDataTo',
    REMOVE_PRICE_POLICIES: 'product.removePricePolicies',
    PARAM_URL : 'product.paramUrl',
    PARAM_URL_CLOSE : 'product.paramUrlClose'
}

const initialState = {
    product: null,
    products: [],
    pricePolicies: [],
    messages: null,
    paramurl: [],
    paramget: false
}

function reducer(state=initialState, action){
    return{
        product: productReducer(state.product, action),
        products: productsReducer(state.products, action),
        pricePolicies: pricePoliciesReducer(state.pricePolicies, action),
        messages: messagesReducer(state.messages, action),
        paramurl: paramUrlReducer(state.paramurl, action),
        paramget: paramUrlSettingReducer(state.paramget, action)
    }
}

function paramUrlReducer(paramurlState, {type, payload}){
    switch(type){
        case actionType.PARAM_URL:
            return paramurlState.concat(payload.id);
        default :
            return paramurlState;
    }
}

function paramUrlSettingReducer(paramGetBoolState, {type, payload}){
    switch(type){
        case actionType.PARAM_URL_CLOSE:
            paramGetBoolState = payload;
            return paramGetBoolState;
        default: 
            return paramGetBoolState;
    }
}

function messagesReducer(messagesState, {type, payload}){
    switch(type){
        case actionType.SET_ERROR_MESSAGE:
            return payload;
        default: return messagesState
    }
};

function pricePoliciesReducer(pricePoliciesState, {type, payload}){
    switch(type){
        case actionType.SET_PRICE_POLICIES:
            return pricePoliciesState.concat(payload);
        case actionType.COMPARE_DATA_TO_FROM:
            var key = payload.key;
            var name = payload.name;
            var data = payload.data;
            var resultKey = pricePoliciesState[key] 
            var toData = resultKey.period.to;
            var fromData = resultKey.period.from;
            var returnSubGroup = {};
            if(toData<=fromData){
                data = 0;
                returnSubGroup={
                    ...resultKey.period,
                    [name] : data
                }
                resultKey.period = returnSubGroup;
            }
            pricePoliciesState[key] = resultKey;
            return [...pricePoliciesState];
        case actionType.CHANGE_PRICE_POLICIES:
            var key = payload.key;
            var categ = payload.categ;
            var name = payload.name;
            var data = payload.data;
            var resultVal = pricePoliciesState[key];
            var resultSubGroup = {}
            if(categ=='period'){
                resultSubGroup = {
                    ...resultVal.period,
                    [name] : data
                }
                resultVal.period = resultSubGroup;
            }
            else
            {
                resultSubGroup = {
                    ...resultVal.price,
                    [name] : data
                }
                resultVal.price = resultSubGroup;
            }
            pricePoliciesState[key] = resultVal;
            return [...pricePoliciesState];
        case actionType.REMOVE_PRICE_POLICIES:
            return payload;
        default : return pricePoliciesState
    }
}

function productsReducer(productsState, {type, payload}){
    switch(type){
        case actionType.SET_PRODUCTS:
            return payload;
        default : return productsState
    }
}

function productReducer(productState, {type, payload}){
    switch(type){
        case actionType.SET_PRODUCT:
            return payload;
        case actionType.CHANGE_PRODUCT_PROP:
            return{
                ...productState,
                [payload.name]: payload.value
            }
        case actionType.CHANGE_PRODUCT_PRICE_POLICIES:
            productState.pricePolicies = payload;
            return productState;
        default: 
            return productState;
    }
}

export default reducer;
export { actionType, reducer };