import React, {Component} from 'react';
import {connect } from 'react-redux';
import autobind from 'autobind-decorator';
import ProductDetailFormView from '../view/ProductListView';
import {bindActionCreators} from 'redux';
import axios from 'axios';

@autobind
class productDetailContainer extends Component {

   
    render(){
       const {} = this.props;
        return(
              <ProductDetailFormView 

              />
        );
    }
}

const mapStateToProps = ({productState, productsState, pricePoliciesState, messagesState}) => ({
    // products: productsState.products,
    // product: productState.product,
    // pricePolicies: pricePoliciesState.pricePolicies,
    // messages: messagesState.messages
});
const mapDispatchToProps = (dispatch) => ({
    // productAction: bindActionCreators(productAction, dispatch),
});

export default connect (mapStateToProps, mapDispatchToProps)(productDetailContainer);