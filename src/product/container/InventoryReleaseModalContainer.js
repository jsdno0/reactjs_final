
import React, { Component } from 'react';

import queryString from 'query-string';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Form, Button } from 'semantic-ui-react';



@autobind
class InventoryReleaseModalContainer extends Component {
    //
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //
    }
    render() {
        return (
            <Modal size="tiny">
                <Modal.Header>Release to Inventory</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Group>
                            <Form.Input
                                placeholder='Quantity'
                            />
                            <Button >Release</Button>
                            <Button >Close</Button>
                        </Form.Group>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

const mapStateToProps = ({  }) => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(InventoryReleaseModalContainer);
