
import React, { Component } from 'react';

import queryString from 'query-string';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Form, Button } from 'semantic-ui-react';


@autobind
class InventoryEnterModalContainer extends Component {
    //
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        //
    }

    render() {
        return (
            <Modal size="tiny">
                <Modal.Header>Enter to Inventory</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Group>
                            <Form.Input
                                placeholder='Quantity'
                            />
                            <Button primary >Enter</Button>
                            <Button >Close</Button>
                        </Form.Group>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

const mapStateToProps = ({  }) => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(InventoryEnterModalContainer);
