import React, {Component} from 'react';
import {connect } from 'react-redux';
import autobind from 'autobind-decorator';
import ProductListView from '../view/ProductListView';
import productAction from '../action/productAction';
import productApi from '../api/productApi';
import {bindActionCreators} from 'redux';
import axios from 'axios';

@autobind
class productListContainer extends Component {

    componentDidMount(){
        productApi.funcFindAllProduct().then(((response) => {
            var productList = response;
            // console.log('productList');
            // console.log(productList);
            this.props.productAction.funcFindAllProduct(productList);
            // productList.map((product, i)=>{
            //     this.props.productAction.funcAddParamUrl(product, i);
            // });
        }));
    }

    componentDidUpdate(){
        // const {products, paramget, paramurl} = this.props;

        // if(paramget==false){
        //     products.map((product, i)=>{
        //         this.props.productAction.funcAddParamUrl(product, i)
        //     });
        //     this.props.productAction.funcCloseParamUrl();
        // }
    }

    funcOnNew(){
        this.props.productAction.funcOnNew();
    }
    funcOnChangeProductProp(name, value){
        this.props.productAction.funcOnChangeProductProp(name, value);
    }
    funcAddPolicy(){
        this.props.productAction.funcAddPolicy();
    }
    funcOnChangePolicy(key,categ,name,data){
        this.props.productAction.funcOnChangePolicy(key,categ,name,data);
        // this.props.productAction.funcCompareWithToFrom(key, name, data);
        this.funcOnChangeProductPricePolicies();
    }
    funcOnChangeProductPricePolicies(){
        const {pricePolicies} = this.props;
        this.props.productAction.funcOnChangeProductPricePolicies(pricePolicies);
    }

    funcSaveProducts(){
        const {product} = this.props;
        this.props.productAction.funcSaveProducts(product);
    }
    funcRemoveAdd(i){
        const {pricePolicies} = this.props;
        var pricePoliciesList = pricePolicies.slice();
        pricePoliciesList.splice(i,1);
        this.props.productAction.funcRemoveAdd(pricePoliciesList);
        this.props.productAction.funcOnChangeProductPricePolicies(pricePoliciesList);
    }

    // funcParaSetting(products){
    //     products.map((product, i)=>{
    //         this.props.productAction.funcAddParamUrl(product, i);
    //     })
    // }
   
    render(){
       const {product, products, pricePolicies} = this.props;
        return(
              <ProductListView 
                product = {product}
                products = {products}
                pricePolicies = {pricePolicies}
                funcOnNew = {this.funcOnNew}
                funcOnChangeProductProp = {this.funcOnChangeProductProp}
                funcAddPolicy = {this.funcAddPolicy}
                funcOnChangePolicy = {this.funcOnChangePolicy}
                funcSaveProducts = {this.funcSaveProducts}
                funcRemoveAdd = {this.funcRemoveAdd}
                funcParaSetting = {this.funcParaSetting}
              />
        );
    }
}
const mapStateToProps = ({productState, productsState, pricePoliciesState, messagesState, paramurlState, paramGetBoolState}) => ({
    products: productsState.products,
    product: productState.product,
    pricePolicies: pricePoliciesState.pricePolicies,
    messages: messagesState.messages,
    paramurl: paramurlState.paramurl,
    paramget: paramGetBoolState.paramget
});
const mapDispatchToProps = (dispatch) => ({
    productAction: bindActionCreators(productAction, dispatch),
});

export default connect (mapStateToProps, mapDispatchToProps)(productListContainer);