
import React, { PureComponent } from 'react';
import { Container, Header, Divider, Item, Button, Table } from 'semantic-ui-react';

import InventoryEnterModalContainer from '../../product/container/InventoryEnterModalContainer';
import InventoryReleaseModalContainer from '../../product/container/InventoryReleaseModalContainer';

class ProductDetailFormView extends PureComponent {
    //    
    constructor(props) {
        super(props);
    }

    render() {
        //
        return (
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                    <Button floated='right' primary >List</Button>
                    <Button floated='right' primary >Inventory</Button>
                    <Button floated='right' color="red" >Release</Button>
                    <Button floated='right' color="green" >Enter</Button>
                </Header>
                <Divider/>
                <Item.Group>
                    <Item>
                        <Item.Content>
                            <Item.Header as='a'>Code > </Item.Header>
                            <Item.Meta>Description</Item.Meta>
                            <Item.Description>
                            </Item.Description>
                            <Item.Extra>Price : </Item.Extra>
                        </Item.Content>
                    </Item>
                </Item.Group>
                Price Policy
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>From</Table.HeaderCell>
                            <Table.HeaderCell>To</Table.HeaderCell>
                            <Table.HeaderCell>Price</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row >
                            <Table.Cell>
                            </Table.Cell>
                            <Table.Cell>
                            </Table.Cell>
                            <Table.Cell>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                <InventoryEnterModalContainer
                />
                <InventoryReleaseModalContainer
                />
            </Container>
            
        )
    }
}

export default ProductDetailFormView;