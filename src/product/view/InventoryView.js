import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider, Table } from 'semantic-ui-react';

import InventoryEnterModalContainer from '../../product/container/InventoryEnterModalContainer';
import InventoryReleaseModalContainer from '../../product/container/InventoryReleaseModalContainer';

class InventoryView extends PureComponent {
    //
    constructor(props) {
        super(props);
    }

    render() {
        //

        return (
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                    Inventory
                    <Button floated='right' primary >Production List</Button>
                </Header>
                <Divider/>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Production Name</Table.HeaderCell>
                            <Table.HeaderCell>Quantity</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row >
                            <Table.Cell> </Table.Cell>
                            <Table.Cell> </Table.Cell>
                            <Table.Cell textAlign="center"> 
                                <Button color="green" >Enter</Button>
                                <Button color="red" >Release</Button>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>No Items</Table.Cell>
                        </Table.Row>
                        }
                    </Table.Body>
                </Table>
                <InventoryEnterModalContainer
                />
                <InventoryReleaseModalContainer
                />
            </Container>
        );
    }
}

export default InventoryView;