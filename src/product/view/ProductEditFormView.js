
import React, { PureComponent } from 'react';
import { Form, Button, Table } from 'semantic-ui-react';
import { funcOnChangeProductProp } from '../action/productAction';

class ProductEditFormView extends PureComponent {
    //

    render() {
        //
        const {
            product
            , pricePolicies
            , funcOnChangeProductProp
            , funcAddPolicy
            , funcOnChangePolicy
            , funcSaveProducts
            , funcRemoveAdd
        } = this.props;

        var count = 0;

        const funcAddPrice = (data) =>{
            let onePoint = {
                "value":data,
            }
            funcOnChangeProductProp('listPrice', onePoint);
        }
        console.log(pricePolicies);
        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Code' placeholder='Code'
                    value={product && product.code}
                    onChange = {(e)=>{funcOnChangeProductProp('code', e.target.value)}}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Name' placeholder='Name'
                    value={product && product.name}
                    onChange = {(e)=>{funcOnChangeProductProp('name', e.target.value)}}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.TextArea label='Description' placeholder='Description'
                    value={product && product.description}
                    onChange = {(e)=>{funcOnChangeProductProp('description', e.target.value)}}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Price' placeholder='Price'
                    value = {product.listPrice.value && product.listPrice.value}
                    onChange = {(e)=>{funcAddPrice(e.target.value)}}
                    />
                </Form.Group>
                <Form.Group>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>From</Table.HeaderCell>
                                <Table.HeaderCell>To</Table.HeaderCell>
                                <Table.HeaderCell>Price</Table.HeaderCell>
                                <Table.HeaderCell textAlign="center">
                                    <Button color="green"
                                        onClick={funcAddPolicy}
                                    >Add</Button>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {
                                Array.isArray(pricePolicies) && pricePolicies.length ? pricePolicies.map((policy, i) => {
                                    count=count+1;
                                    return (
                                        <Table.Row 
                                            key={count-1}
                                            id={count-1}>
                                            <Table.Cell>
                                                <Form.Input fluid placeholder='From'
                                                value = {policy.period && policy.period.from}
                                                onChange = {(e)=>{funcOnChangePolicy(i, 'period', 'from', e.target.value)}}
                                                />
                                            </Table.Cell>
                                            <Table.Cell>
                                                <Form.Input fluid placeholder='To'
                                                value = {policy.period && policy.period.to}
                                                onChange = {(e)=>{funcOnChangePolicy(i, 'period', 'to', e.target.value)}}
                                                />
                                            </Table.Cell>
                                            <Table.Cell>
                                                <Form.Input fluid placeholder='Price'
                                                value = {policy.price && policy.price.value}
                                                onChange = {(e)=>{funcOnChangePolicy(i, 'price', 'value', e.target.value)}}
                                                />
                                            </Table.Cell>
                                            <Table.Cell textAlign="center">
                                                <Button color="red"
                                                    onClick={()=>{funcRemoveAdd(i)}}
                                                >Remove</Button>
                                            </Table.Cell>
                                        </Table.Row>
                                    );
                                }) :
                                    <Table.Row>
                                        <Table.Cell>
                                            Please Click Add Button
                                        </Table.Cell>
                                    </Table.Row>
                            }
                        </Table.Body>
                    </Table>
                </Form.Group>
                <Button primary
                onClick={funcSaveProducts}
                >Save</Button>
                <Button secondary >Cancel</Button>
            </Form>
        )
    }
}

export default ProductEditFormView;