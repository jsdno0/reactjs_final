
import React, { PureComponent } from 'react';
import { Table } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class ProductListTableView extends PureComponent {
    //
    render() {
        //
        const {products} = this.props;
        console.log(products);
        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Code</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {                        
                        products && 
                        Array.isArray(products)&&products.length?products.map((product)=>{
                            return(
                            <Table.Row key={product.id}>
                                
                                <Table.Cell style={{cursor: 'pointer'}}><Link to="/front/product/:product.id">{product.code}</Link></Table.Cell>
                                <Table.Cell style={{cursor: 'pointer'}}><Link to="/front/product/:product.id">{product.name}</Link></Table.Cell>
                                
                            </Table.Row>
                            );
                        })
                        :                    
                        <Table.Row>
                            <Table.Cell>No Items</Table.Cell>
                        </Table.Row>
                    }
                </Table.Body>
            </Table>
        );
    }
}

export default ProductListTableView;