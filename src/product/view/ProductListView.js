
import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider } from 'semantic-ui-react';

import ProductListTableView from './ProductListTableView';
import ProductEditFormView from './ProductEditFormView';

class ProductListView extends PureComponent {
    //
    render() {
        //
        const {
            product
            , products
            , pricePolicies
            , funcOnNew
            , funcOnChangeProductProp
            , funcAddPolicy
            , funcOnChangePolicy
            , funcSaveProducts
            , funcRemoveAdd
            , funcParaSetting
        } = this.props;

        return (
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                    Product List
                    <Button floated='right' primary >Inventory</Button>
                    <Button floated='right' primary 
                        onClick={funcOnNew}
                    >New</Button>
                </Header>
                <Divider/>
                {
                    product &&
                    <ProductEditFormView
                        product = {product}
                        products = {products}
                        pricePolicies = {pricePolicies}
                        funcOnChangeProductProp = {funcOnChangeProductProp}
                        funcAddPolicy = {funcAddPolicy}
                        funcOnChangePolicy = {funcOnChangePolicy}
                        funcSaveProducts = {funcSaveProducts}
                        funcRemoveAdd = {funcRemoveAdd}
                    />
                }
                
                <Divider/>
                <ProductListTableView
                    products = {products}
                />
            </Container>
        );
    }
}

export default ProductListView;