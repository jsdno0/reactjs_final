import { actionType } from '../reducer/productReducer';
import productApi from '../api/productApi';

function funcFindAllProduct(products) {
    return (dispatch) => dispatch({
        type: actionType.SET_PRODUCTS,
        payload: products
    })
}

function funcOnNew() {
    return (dispatch) => dispatch({
        type: actionType.SET_PRODUCT,
        payload: {
            "code": '',
            "name": '',
            "description": '',
            "listPrice": {
                "value": ''
            },
            "pricePolicies": []
        }
    });
}

function funcOnChangeProductProp(name, value) {
    return (dispatch) => dispatch({
        type: actionType.CHANGE_PRODUCT_PROP,
        payload: { name, value }
    });
}

function funcOnChangeProductPricePolicies(pricePolicies){
    return (dispatch) => dispatch({
        type: actionType.CHANGE_PRODUCT_PRICE_POLICIES,
        payload: pricePolicies
    });
}

function funcAddPolicy(){
    return (dispatch) => dispatch({
        type: actionType.SET_PRICE_POLICIES,
        payload: {
            "period": {
                "from": '',
                "to": ''
            },
            "price": {
                "value": ''
            }
        }
    })
}

function funcOnChangePolicy(key,categ,name,data){
    return (dispatch) => dispatch({
        type: actionType.CHANGE_PRICE_POLICIES,
        payload: {key, categ, name, data}
    })
}

function funcCompareWithToFrom(key, name, data){
    return(dispatch) => dispatch({
        type: actionType.COMPARE_DATA_TO_FROM,
        payload: {key, name, data}
    });
}

function funcSaveProducts(product){
    return () => productApi.funcSaveProducts(product);
}

function funcRemoveAdd(pricePolicies){
    return (dispatch) => dispatch({
        type: actionType.REMOVE_PRICE_POLICIES,
        payload: pricePolicies
    });
}

function funcAddParamUrl(product, i){
    return (dispatch)=>dispatch({
        type: actionType.PARAM_URL,
        payload: product
    })
}

function funcCloseParamUrl(){
    return (dispatch)=>dispatch({
        type: actionType.PARAM_URL_CLOSE,
        payload: true
    })
}

export default {
    funcOnNew,
    funcFindAllProduct,
    funcOnChangeProductProp,
    funcAddPolicy,
    funcOnChangePolicy,
    funcOnChangeProductPricePolicies,
    funcCompareWithToFrom,
    funcSaveProducts,
    funcRemoveAdd,
    funcAddParamUrl,
    funcCloseParamUrl,
}

export {
    funcOnNew,
    funcFindAllProduct,
    funcOnChangeProductProp,
    funcAddPolicy,
    funcOnChangePolicy,
    funcOnChangeProductPricePolicies,
    funcCompareWithToFrom,
    funcSaveProducts,
    funcRemoveAdd,
    funcAddParamUrl,
    funcCloseParamUrl,
}