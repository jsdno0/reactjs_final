const Products = [
    {
        sn : 'A0001',
        productName : 'Mac Book',
        vendor : 'Apple',
        price : 1690000,
        imgUrl : 'images/mac.png',
        detail : 'Retina display, 1.2GHz dual-core Intel Core m3, 8GB of 1866MHz LPDDR3 onboard memory...'
    },
    {
        sn : 'A0002',
        productName : 'gram',
        vendor : 'LG',
        price : 2050000,
        imgUrl : 'images/gram.png',
        detail : '15.6 Ultra-Lightweight Touchscreen Laptop w/ Intel® Core™ i7 processor and Thunderbolt™ 3...'
    },
    {
        sn : 'A0003',
        productName : 'Notebook 9 pro',
        vendor : 'Samsung',
        price : 1850000,
        imgUrl : 'images/samsung.png',
        detail : 'Windows 10 Home, 8th Gen Intel® Core™ i7 Processor, 15" FHD LED Display (1920x1080 dots), 256GB SSD Storage...'
    },
    {
        sn : 'A0004',
        productName : 'XPS 15',
        vendor : 'Dell',
        price : 2599000,
        imgUrl : 'images/dell.png',
        detail : 'Our most powerful 15-inch 2-in-1. Built with the first-ever quad-core 8th Gen Intel® Core™ processor with Radeon™ Vega M discrete graphics on a single chip....'
    }
]

export default Products;